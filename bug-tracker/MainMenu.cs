﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bug_tracker
{
    public partial class MainMenu : Form
    {
        // cerastion of my database connections. for use with mutipule tables.
        DatabaseConnection objConnect;
        DatabaseConnection objConnect1;
        DatabaseConnection objConnect2;
        // declerastion of my datasets used for me data.
        string conString;
        DataSet d;
        DataSet ds;
        DataSet ds1;
        DataSet ds2;
        Form1 Userspop1;
        //Set up of veriables used with in this form.
        public int MaxRows;
        int Openbugs;
        int Closedbugs;
        int inc = 0;
        public int projectid = 0;
        string id1;
        public int rowcount;

        //Initialize of the program and sets Username and ID to values i can use in the rest of the form.
        public MainMenu(string name, string id)
        {
           InitializeComponent();
           label2.Text = name;
           LoadTable();
           textBox2.Text = id;
           id1 = id;
           loadbugsummry();

            if (id == "1") {
                button1.Visible = true;
            }
        }

        // this Makes the ueser tables Appear in a new FORM
        private void button1_Click(object sender, EventArgs e)
        {
            Userspop1 = new Form1();
            Userspop1.Show();
        }

        // This will load my Tables, and display them in a datagrid view
        public void LoadTable()
        {
            conString = Properties.Settings.Default.EmployeesConnectionString;
            SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM projects Inner Join tbl_employees on projects.UserID=tbl_employees.id", conString);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            dataGridView1.DataSource = dataTable;
            dataGridView1.CurrentCell = this.dataGridView1[2, inc];
            dataGridView1.Columns["id"].Visible = false;
            dataGridView1.Columns["userid"].Visible = false;
            dataGridView1.Columns["id1"].Visible = false;
            dataGridView1.Columns["password"].Visible = false;
            dataGridView1.Columns["username"].Visible = false;

            objConnect = new DatabaseConnection();
            objConnect.connection_string = conString;
            objConnect.Sql = "SELECT * FROM projects";
            d = objConnect.GetConnection;

            rowcount = dataTable.Rows.Count;
        }


        // loads the correct bug summry informastion when the Project is selected.
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        { 
            projectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells["ID"].Value);
            loadbugsummry();
        }

        // a methord i cearted to load a qwick summary view of the bugs in a project.
        public void loadbugsummry() {

            conString = Properties.Settings.Default.EmployeesConnectionString;

            objConnect = new DatabaseConnection();
            objConnect.connection_string = conString;
            objConnect.Sql = "SELECT * FROM bugs  Where projectid='" + projectid + "'";
            ds = objConnect.GetConnection;
            MaxRows = ds.Tables[0].Rows.Count;

            objConnect1 = new DatabaseConnection();
            objConnect1.connection_string = conString;
            objConnect1.Sql = "SELECT * FROM bugs  Where projectid='" + projectid + "' and status='Open'";
            ds1 = objConnect1.GetConnection;
            Openbugs = ds1.Tables[0].Rows.Count;

            objConnect2 = new DatabaseConnection();
            objConnect2.connection_string = conString;
            objConnect2.Sql = "SELECT * FROM bugs  Where projectid='" + projectid + "' and status='Close'";
            ds2 = objConnect2.GetConnection;
            Closedbugs = ds2.Tables[0].Rows.Count;

            //MessageBox.Show(MaxRows.ToString());
            totalnobug.Text = MaxRows.ToString();
            openbugs.Text = Openbugs.ToString();
            closebugs.Text = Closedbugs.ToString();
        }

        // when a project is dobble clicked it sends the Project ID to the Bugs Form i cerated, and also loads the form.
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int projectid = 0;
            projectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells["ID"].Value);
            Bugs Userspops;
            Userspops = new Bugs(projectid);
            Userspops.Show();
            
        }

        // used to incert new projects into the Projects Table.
        public bool insert()
        {
            string company = textBox2.Text;
            string title = textBox1.Text;
            string type = comboBox1.Text;

            if (title.Length > 0 && type.Length > 0 && company.Length > 0)
            {
                objConnect = new DatabaseConnection();
                objConnect.connection_string = conString;
                objConnect.Sql = "SELECT * FROM projects";
                d = objConnect.GetConnection;

                DataRow row = d.Tables[0].NewRow();

                row[1] = textBox2.Text;
                row[2] = textBox1.Text;
                row[3] = comboBox1.Text;

                d.Tables[0].Rows.Add(row);

                try
                {
                    objConnect.UpdateDatabase(d);
                    LoadTable();
                    MessageBox.Show("Database updated");


                }
                catch (Exception err)
                {

                    MessageBox.Show(err.Message);

                }
                return true;
            }
            else
            {
                return false;
            }
        }

        // used to incert new projects into the Projects Table.
        private void button4_Click(object sender, EventArgs e)
        {
            insert();
        }

        //loads all the projects when the view all projects button is clicked.
        private void button3_Click(object sender, EventArgs e)
        {
            LoadTable();
            projectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells["ID"].Value);
            loadbugsummry();
        }


        //loads all the projects the user has cerated ONLY.
        private void button2_Click(object sender, EventArgs e)
        {
            conString = Properties.Settings.Default.EmployeesConnectionString;
            SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM projects Where userid='" + id1 + "'", conString);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            dataGridView1.DataSource = dataTable;
            dataGridView1.CurrentCell = this.dataGridView1[2, inc];
            dataGridView1.Columns["id"].Visible = false;
            dataGridView1.Columns["userid"].Visible = false;
            projectid = Convert.ToInt32(dataGridView1.CurrentRow.Cells["ID"].Value);
            loadbugsummry();

        }
    }
}
