﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace bug_tracker
{
    public partial class Login : Form
    {
        // Variables are decalerd hear
        MainMenu Userspops;
        string conString;
        public Login()
        {
            InitializeComponent();
            textBox1.UseSystemPasswordChar = true;
        }
        // used to close the form
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //used as a submit button to run the login methord.
        private void button2_Click(object sender, EventArgs e)
        {
            login();
        }

        //this check box will show the text in the password box for easy of use.
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
            textBox1.UseSystemPasswordChar = false; }
            else { textBox1.UseSystemPasswordChar = true;}
        }

        //the login methord checks the user name and password for a match in the database, if a row is retuned then acsses is granted.
        //els it will show a message box proting the user to check there details.
        public bool login() {
            conString = Properties.Settings.Default.EmployeesConnectionString;

            SqlConnection conn = new SqlConnection(conString);
            SqlDataAdapter sdaa = new SqlDataAdapter("Select first_name,Id from tbl_employees Where username='" + textBox2.Text + "' and password='" + textBox1.Text + "'", conn);
            DataTable dt = new System.Data.DataTable();
            sdaa.Fill(dt);

            if (dt.Rows.Count == 1)
            {
                Userspops = new MainMenu(dt.Rows[0][0].ToString(), dt.Rows[0][1].ToString());
                Userspops.Show();
                Hide();
                return true;
            }
            else {
                MessageBox.Show("Please Check Login Details");
                return false;
            }

        }
        // this allows the user to press the Enter Key when in the password box, so you do not have to click login to submit.
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                login();
            }
            
        }
    }
}
