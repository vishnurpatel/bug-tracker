﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using bug_tracker;
namespace BugsTests
{
    /// <summary>
    /// Summary description for BugsTest
    /// </summary>
    [TestClass]
    public class BugsTest
    {
        [TestMethod]
        public void Project_Pullcode_table_pass()
        {
            Bugs form = new Bugs(2);
            int actule;
            int expected;
            //should pass becase there are two rows of code data assoiated with the Project ID of 2.


            actule = form.codecount;
            expected = 2;

            Assert.AreEqual(expected, actule);

        }
        [TestMethod]
        public void Project_bugs_table_pass()
        {
            Bugs form = new Bugs(2);
            int actule;
            int expected;
            //should pass becase there are two rows of bug data assoiated with the Project ID of 2.


            actule = form.bugscount;
            expected = 2;

            Assert.AreEqual(expected, actule);

        }
        [TestMethod]
        public void Project_bugs_linecounter()
        {
            Bugs form = new Bugs(2);
            int actule;
            int expected;
            //should pass becase there are 3 rows of test the linecounter methord should update my veriable linecounter1 correctly.
            //this methrod helps populate the line numbers in my text viwer

            form.richTextBox1.Text = "Test" + "\n" + "\n";
            form.linecount();
            actule = form.linecounter1;
            expected = 3;

            Assert.AreEqual(expected, actule); 

        }
    }
}
