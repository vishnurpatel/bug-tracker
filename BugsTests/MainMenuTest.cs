﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using bug_tracker;

namespace BugsTests
{
    /// <summary>
    /// Summary description for MainMenuTest
    /// </summary>
    [TestClass]
    public class MainMenuTest
    {
        [TestMethod]
        public void Project_insert_pass()
        {
            MainMenu form = new MainMenu("Vishnu", "3");
            bool actule;
            bool expected;
            //should pass becase the feilds match my rules.
            form.textBox2.Text = "1";
            form.textBox1.Text = "test";
            form.comboBox1.Text = "test";

            actule = form.insert();
            expected = true;

            Assert.AreEqual(expected, actule);

        }

        [TestMethod]
        public void Project_insert_fail()
        {
            MainMenu form = new MainMenu("Vishnu", "3");
            bool actule;
            bool expected;
            //should pass becase the 1st feild is empty. and i am expeting the result to be false
            form.textBox2.Text = "";
            form.textBox1.Text = "TEST";
            form.comboBox1.Text = "TEST";

            actule = form.insert();
            expected = false;

            Assert.AreEqual(expected, actule);

        }


        [TestMethod]
        public void Project_bugsummry_pass()
        {
            MainMenu form = new MainMenu("Vishnu", "1");
            int actule;
            int expected;
            //should pass becase the ammount of bugs in project ID 2 is 2.
            form.projectid = 2;
            form.loadbugsummry();
            actule = form.MaxRows;
            expected = 2;

            Assert.AreEqual(expected, actule);

        }


        [TestMethod]
        public void Project_bugsummry_fail()
        {
            MainMenu form = new MainMenu("Vishnu", "1");
            int actule;
            int expected;
            //should pass becase the ammount of bugs in project ID 2 is 2. and i am expting them not to be Equal

            form.projectid = 2;
            form.loadbugsummry();
            actule = form.MaxRows;
            expected = 1;

            Assert.AreNotEqual(expected, actule);

        }

        [TestMethod]
        public void MainMenu_table_load()
        {
            MainMenu form = new MainMenu("Vishnu", "1");
            int actule;
            int expected;
            //this just tests that it pulls the correct ammount of projects from the table, atm the project count is at 5

            form.LoadTable();
            actule = form.rowcount;
            expected = 5;

            Assert.AreEqual(expected, actule);

        }
    }
}

