﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using bug_tracker;
namespace BugsTests
{
    [TestClass]
    public class LoginTest
    {
        [TestMethod]
        public void login_test_pass()
        {
            Login form = new Login();
            bool actule;
            bool expected;

            form.textBox2.Text = "admin";
            form.textBox1.Text = "a15vrp";
            // should pass due to there are login details with the name admin, and Password a15vrp
            actule = form.login();
            expected = true;

            Assert.AreEqual(expected, actule);

        }
        [TestMethod]
        public void login_test_fail()
        {
            Login form = new Login();
            bool actule;
            bool expected;
            // should pass the fail due to the login details are incorrect            
            form.textBox2.Text = "admin";
            form.textBox1.Text = "password";

            actule = form.login();
            expected = false;

            Assert.AreEqual(expected, actule);

        }
    }
}
